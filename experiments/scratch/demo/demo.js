var Scene = require('../Scene.js');

var container = document.getElementById('container');
var scene = new Scene(window.innerWidth, window.innerHeight, container);

scene.setBackground('demo/images/bg.jpg');
scene.initGrid();
scene.animate();
