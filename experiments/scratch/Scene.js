'use strict';

var PIXI = require('pixi.js');
var Tween = require('gsap');


function Scene (w, h, container) {
  this.stage = new PIXI.Stage();
  this.renderer = new PIXI.WebGLRenderer(w, h);
  this.BOX_SIZE = 100;
  container.appendChild(this.renderer.view);
}

Scene.prototype.setBackground = function(path) {
  var bg = PIXI.Sprite.fromImage(path);
  bg.width = this.renderer.width;
  bg.height = this.renderer.height;
  this.stage.addChild(bg);
};

Scene.prototype.initGrid = function() {
  var boxesInWidth = Math.ceil(this.renderer.width / this.BOX_SIZE);
  var boxesInHeight = Math.ceil(this.renderer.height / this.BOX_SIZE);
  var r;
  for (var i = 0; i < boxesInWidth; i++) {
    for (var j = 0; j < boxesInHeight; j++) {
      r = _createRect.call(this, i * this.BOX_SIZE, j * this.BOX_SIZE);
      this.stage.addChild(r);
    }
  }
};

Scene.prototype.animate = function() {
  requestAnimationFrame(this.animate.bind(this));
  this.renderer.render(this.stage);
};


function _createRect (x, y) {
  var g = new PIXI.Graphics();
  g.interactive = true;
  g.beginFill(0x434343, 1);
  g.drawRect(x, y, this.BOX_SIZE, this.BOX_SIZE);
  g.endFill();
  g.mouseover = function(e) {
    Tween.killTweensOf(e.target);
    e.target.alpha = 0;
  };
  g.mouseout = function(e) {
    Tween.to(e.target, 1.0, {alpha: 1});
  };

  return g;
}

module.exports = Scene;
