'use strict'

var inherits = require('inherits');
var getPixels = require('get-image-pixels');
var Color = require('color');
var palette = require('palette');
var PIXI = require('pixi.js');

function StripeImage(url) {
  PIXI.DisplayObjectContainer.call(this);

  this.image = new Image();
  this.image.onload = this.onImageLoaded.bind(this);
  this.image.src = url;

  this.sprite = PIXI.Sprite.fromImage(url);
  this.addChild(this.sprite);
}

inherits(StripeImage, PIXI.DisplayObjectContainer);


StripeImage.prototype.onImageLoaded = function(image) {
  this.baseImageWidth = this.image.width;
  this.baseImageHeight = this.image.height;

  this.image.width *= 0.1;
  this.image.height *= 0.1;

  this.slicesBack = [];
  this.slicesFront = [];

  this.tl = new TimelineMax();

  var canvas = document.createElement('canvas');
  var ctx = canvas.getContext('2d');
  canvas.width = this.baseImageWidth;
  canvas.height = this.baseImageHeight;
  ctx.fillStyle = 'white';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.drawImage(this.image, 0, 0);

  var pal = palette(canvas, 10);
  var nbStripe = pal.length;
  var stripeWidth = this.baseImageWidth / nbStripe;
  var sliceBack, sliceFront, color;

  for(var i = 0; i < nbStripe; i++) {
    color = parseInt(Color().rgb(pal[i]).hexString().replace('#', ''), 16);

    sliceBack = new PIXI.Graphics();
    sliceBack.beginFill(color, 0.5);
    sliceBack.drawRect(0, 0, stripeWidth, this.baseImageHeight);
    sliceBack.endFill();
    sliceBack.x = i * stripeWidth;
    sliceBack.y = 0;
    // sliceBack.pivot.y = this.baseImageHeight;

    sliceFront = new PIXI.Graphics();
    sliceFront.beginFill(color, 1);
    sliceFront.drawRect(0, 0, stripeWidth, this.baseImageHeight * 0.2);
    sliceFront.x = sliceBack.x;
    sliceFront.y = 0;
    // sliceFront.pivot.y = this.baseImageHeight * 0.2;

    this.slicesBack.push(sliceBack);
    this.addChild(sliceBack);

    this.slicesFront.push(sliceFront);
    this.addChild(sliceFront);

    this.tl.fromTo(sliceBack.scale, 1, {y: 0}, {y: 1, ease: Expo.easeOut}, i * 0.05);
    this.tl.fromTo(sliceFront.scale, 1, {y: 0}, {y: 1, ease: Expo.easeOut}, i * 0.05 + 0.25);
  }

  this.tl.pause(0);

  // this.tl.staggerFromTo(this.slicesBack, 1, {scale: {x: 0.5, y: 0.5}}, {scale: {x: 1, y: 1}, ease: Expo.easeOut}, 0.1, 0);

  this.sprite.interactive = true;
  this.sprite.mouseover = this.onMouseOver.bind(this);
  this.sprite.mouseout = this.onMouseOut.bind(this);
};

StripeImage.prototype.onMouseOver = function () {
  this.tl.play();
};

StripeImage.prototype.onMouseOut = function () {
  this.tl.reverse();
};

module.exports = StripeImage;