'use strict'

var raf = require('raf');
var resize = require('brindille-resize');
var getPixels = require('get-image-pixels');
var StripeImage = require('./StripeImage');
var Color = require('color');
var PIXI = require('pixi.js');

function App() {
  this.renderer = new PIXI.WebGLRenderer(1280, 720, {
    antialias: true
  });
  this.stage = new PIXI.Stage(0xFFFFFF);

  var nbCol = Math.ceil(window.innerWidth / 480);
  var image;
  for(var i = 0, l = 16; i < l; i++) {
    image = new StripeImage('assets/images/img' + i + '.jpg');
    image.x = 480 * (i % nbCol);
    image.y = 270 * Math.floor(i / nbCol);
    this.stage.addChild(image);
  }

  // this.image = new StripeImage('assets/images/1.jpg');
  // this.stage.addChild(this.image);

  resize.addListener(this.resize.bind(this));

  raf(this.animate.bind(this));
}

App.prototype.resize = function() {
  this.renderer.resize(resize.width, resize.height);
  // this.image.x = this.renderer.width * 0.5 - 480 * 0.5;
  // this.image.y = this.renderer.height * 0.5 - 270 * 0.5;
};

App.prototype.animate = function() {
  raf(this.animate.bind(this));

  this.renderer.render(this.stage);
};

module.exports = App;