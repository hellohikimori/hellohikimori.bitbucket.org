'use strict'

var preloader = require('brindille-preloader');
var PrismHover = require('./PrismHover');
var PrismHover2 = require('./PrismHover2');
var PrismHover3 = require('./PrismHover3');

function App() {
  preloader.on('complete', this.allImagesLoaded.bind(this));
  preloader.load([
    {id: 'thumb0', src: 'assets/images/thumb0.jpg'},
    {id: 'thumb1', src: 'assets/images/thumb1.jpg'},
    {id: 'thumb2', src: 'assets/images/thumb2.jpg'},
    {id: 'mask0', src: 'assets/images/mask/0.png'},
    {id: 'mask1', src: 'assets/images/mask/1.png'},
    {id: 'mask2', src: 'assets/images/mask/5.png'},
    {id: 'mask3', src: 'assets/images/mask/4.png'}
  ]);
}

App.prototype.allImagesLoaded = function() {
  this.prism1 = new PrismHover({
    width: 480,
    height: 270,
    container: document.getElementById('view'),
    image: preloader.getImage('thumb0'),
    masks: [
      preloader.getImage('mask0'),
      preloader.getImage('mask1'),
      preloader.getImage('mask2'),
      preloader.getImage('mask3')
    ]
  });

  this.prism2 = new PrismHover2({
    width: 480,
    height: 270,
    container: document.getElementById('view2'),
    image: preloader.getImage('thumb1'),
    masks: [
      preloader.getImage('mask0'),
      preloader.getImage('mask1'),
      preloader.getImage('mask2'),
      preloader.getImage('mask3')
    ]
  });

  this.prism3 = new PrismHover3({
    width: 480,
    height: 270,
    container: document.getElementById('view3'),
    image: preloader.getImage('thumb2'),
    masks: [
      preloader.getImage('mask0'),
      preloader.getImage('mask1'),
      preloader.getImage('mask2'),
      preloader.getImage('mask3')
    ]
  });
};
  
module.exports = App;