'use strict';

function PrismHover(options) {
  this.masks = options.masks;
  this.container = options.container;
  this.width = options.width;
  this.height = options.height;
  this.image = options.image;
  this.canvases = [];

  this.offsets = [
    {
      x: 0,
      y: 0
    },
    {
      x: 0,
      y: 0
    },
    {
      x: 0,
      y: 0
    }
  ];

  for(var i = 0; i < this.masks.length; i++) {
    var canvas = this.addCanvas()
    this.canvases.push(canvas);
    canvas.canvas.addEventListener('mouseover', this.onMouseOver.bind(this));
    canvas.canvas.addEventListener('mouseout', this.onMouseOut.bind(this));
  }

  this.render();
};

PrismHover.prototype.onMouseOver = function(e) {
  TweenMax.killTweensOf(this.offset);
  TweenMax.to(this.offsets[0], 0.5, {y: -40, x: 0, ease: Expo.easeOut, onUpdate: this.render, onUpdateScope: this});
  TweenMax.to(this.offsets[1], 0.5, {y: 40, x: 0, ease: Expo.easeOut});
  TweenMax.to(this.offsets[2], 0.5, {y: 0, x: 40, ease: Expo.easeOut});
};

PrismHover.prototype.onMouseOut = function(e) {
  TweenMax.killTweensOf(this.offset);
  TweenMax.to(this.offsets[0], 0.5, {y: 0, x: 0, ease: Expo.easeOut, onUpdate: this.render, onUpdateScope: this});
  TweenMax.to(this.offsets[1], 0.5, {y: 0, x: 0, ease: Expo.easeOut});
  TweenMax.to(this.offsets[2], 0.5, {y: 0, x: 0, ease: Expo.easeOut});
};

PrismHover.prototype.clearAll = function() {
  for(var i = 0, l = this.canvases.length; i < l; i++) {
    this.canvases[i].context.clearRect(0, 0, this.canvases[i].canvas.width, this.canvases[i].canvas.height);
  }
};

PrismHover.prototype.render = function() {
  this.clearAll();
  this.renderImage(this.canvases[0].context, this.masks[3], 'source-over');
  this.renderImage(this.canvases[0].context, this.image, 'source-atop', 0);
  this.renderImage(this.canvases[0].context, this.image, 'destination-over');

  this.renderImage(this.canvases[1].context, this.masks[2], 'source-over');
  this.renderImage(this.canvases[1].context, this.image, 'source-atop', 1);
  
  this.renderImage(this.canvases[2].context, this.masks[1], 'source-over');
  this.renderImage(this.canvases[2].context, this.image, 'source-atop', 2);
};

PrismHover.prototype.addCanvas = function() {
  var obj = {
    canvas: document.createElement('canvas'),
    context: null
  };
  obj.context = obj.canvas.getContext('2d');
  obj.canvas.width = this.width;
  obj.canvas.height = this.height;
  this.container.appendChild(obj.canvas);
  return obj;
};

PrismHover.prototype.addMask = function(mask) {
  this.loadImage(this.mask.url, this.renderMask.bind(this));
};

PrismHover.prototype.renderMask = function() {
  
};

PrismHover.prototype.renderImage = function(context, image, composite, offsetIndex) {
  if(composite) context.globalCompositeOperation = composite;
  context.save();
  if(composite === 'source-atop') {
    context.translate(this.offsets[offsetIndex].x, this.offsets[offsetIndex].y);
  }
  context.drawImage(image, 0, 0, this.width, this.height);
 
  // if(composite === 'source-atop') {
  //   this.context.globalAlpha = this.offset.alpha;
  //   this.context.fillStyle = '#000000'; 
  //   this.context.fillRect(0, 0, this.width, this.height);
  //   this.context.globalAlpha = 1;
  // }
  context.restore();
};

PrismHover.prototype.loadImage = function(url, callback) {
  var image = new Image();
  image.onload = callback;
  image.src = url;
  return image;
};

module.exports = PrismHover;