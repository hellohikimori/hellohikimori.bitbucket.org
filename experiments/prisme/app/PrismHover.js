'use strict';

function PrismHover(options) {
  this.masks = options.masks;
  this.container = options.container;
  this.width = options.width;
  this.height = options.height;
  this.image = options.image;

  this.offset = {
    x: 0,
    y: 0
  };

  this.addCanvas();
  this.render();
  this.canvas.addEventListener('mouseover', this.onMouseOver.bind(this));
  this.canvas.addEventListener('mouseout', this.onMouseOut.bind(this));
};

PrismHover.prototype.onMouseOver = function(e) {
  TweenMax.killTweensOf(this.offset);
  TweenMax.to(this.offset, 0.5, {y: -20, x: 0, ease: Expo.easeOut, onUpdate: this.render, onUpdateScope: this});
};

PrismHover.prototype.onMouseOut = function(e) {
  TweenMax.killTweensOf(this.offset);
  TweenMax.to(this.offset, 0.5, {y: 0, x: 0, ease: Expo.easeOut, onUpdate: this.render, onUpdateScope: this});
};

PrismHover.prototype.render = function() {
  this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  this.renderImage(this.masks[0], 'source-over');
  this.renderImage(this.image, 'source-atop', 0);
  this.renderImage(this.image, 'destination-over');
};

PrismHover.prototype.addCanvas = function() {
  this.canvas = document.createElement('canvas');
  this.context = this.canvas.getContext('2d');
  this.canvas.width = this.width;
  this.canvas.height = this.height;
  this.container.appendChild(this.canvas);
};

PrismHover.prototype.addMask = function(mask) {
  this.loadImage(this.mask.url, this.renderMask.bind(this));
};

PrismHover.prototype.renderMask = function() {
  
};

PrismHover.prototype.renderImage = function(image, composite, offsetIndex) {
  if(composite) this.context.globalCompositeOperation = composite;
  this.context.save();
  if(composite === 'source-atop') {
    this.context.translate(this.offset.x, this.offset.y);
  }
  this.context.drawImage(image, 0, 0, this.width, this.height);
 
  // if(composite === 'source-atop') {
  //   this.context.globalAlpha = this.offset.alpha;
  //   this.context.fillStyle = '#000000'; 
  //   this.context.fillRect(0, 0, this.width, this.height);
  //   this.context.globalAlpha = 1;
  // }
  this.context.restore();
};

PrismHover.prototype.loadImage = function(url, callback) {
  var image = new Image();
  image.onload = callback;
  image.src = url;
  return image;
};

module.exports = PrismHover;