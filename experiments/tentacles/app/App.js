'use strict'

var raf = require('raf');
var distance = require('./maths/distance');
var Rectangle = require('./maths/rectangle');
var Tentacle = require('./objects/Tentacle');

function App() {
  var nbTentacles = 50;
  var nucleusRadius = 30;

  this.canvas = document.getElementById("canvas");
  this.context = this.canvas.getContext("2d");
  this.canvas.width = window.innerWidth;
  this.canvas.height = window.innerHeight;
  
  this.tentacles = [];
  this.tentacleParams = {
    boundaries: new Rectangle(0, 0, this.canvas.width, this.canvas.height),
    bounce: 1,
    friction: 0
  };

  for(var i = 0; i < nbTentacles; i++) {
    var angle = i + 1 * (Math.PI * 2 / nbTentacles);
    this.tentacles.push(new Tentacle({
      boundaries: this.tentacleParams.boundaries,
      bounce: this.tentacleParams.bounce,
      friction: this.tentacleParams.friction,
      angle: angle,
      origin: {
        x: this.canvas.width * 0.5 + Math.cos(angle) * nucleusRadius,
        y: this.canvas.height * 0.5 + Math.sin(angle) * nucleusRadius
      }
    }));
  }
  
  raf(this.update.bind(this));
}
 
App.prototype.update = function() {
  raf(this.update.bind(this));

  this.context.clearRect(0, 0, this.tentacleParams.boundaries.width, this.tentacleParams.boundaries.height);
  for(var i = 0, l = this.tentacles.length; i < l; i++) {
    this.tentacles[i].update();
    this.tentacles[i].render(this.context);
  }
};

module.exports = App;