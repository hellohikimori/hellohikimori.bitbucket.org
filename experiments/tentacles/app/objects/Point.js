'use strict';

function Point(options) {
  this.x = options.x || 0;
  this.y = options.y || 0;
  this.oldx = options.oldx || this.x + Math.random() * 20 - 10;
  this.oldy = options.oldy || this.y + Math.random() * 20 - 10;
  this.fixed = options.fixed;
  this.color = options.color || '#FF6230';
}

Point.prototype.update = function(friction, gravity) {
  if(this.fixed) return;

  var vx = (this.x - this.oldx) * friction;
  var vy = (this.y - this.oldy) * friction;

  this.oldx = this.x;
  this.oldy = this.y;
  this.x += vx;
  this.y += vy;
  this.x += gravity.x;
  this.y += gravity.y;
};

Point.prototype.applyConstraints = function(boundaries, friction, bounce) {
  if(this.fixed) return;

  var vx = (this.x - this.oldx) * friction;
  var vy = (this.y - this.oldy) * friction;

  if(this.x > boundaries.width) {
    this.x = boundaries.width;
    this.oldx = this.x + vx * bounce;
  }
  else if(this.x < 0) {
    this.x = 0;
    this.oldx = this.x + vx * bounce;
  }
  if(this.y > boundaries.height) {
    this.y = boundaries.height;
    this.oldy = this.y + vy * bounce;
  }
  else if(this.y < 0) {
    this.y = 0;
    this.oldy = this.y + vy * bounce;
  }
};

Point.prototype.render = function(context) {
  context.beginPath();
  context.fillStyle = this.color;
  context.arc(this.x, this.y, 1, 0, Math.PI * 2);
  context.closePath();
  context.fill();
};

module.exports = Point;