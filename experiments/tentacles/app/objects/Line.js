'use strict';

var distance = require('../maths/distance');
var Point = require('./Point');

function Line(options) {
  this.p0 = options.p0 || new Point();
  this.p1 = options.p1 || new Point();
  this.length = distance(this.p0, this.p1);
  this.color = options.color || '#545454';
  this.thickness = options.thickness || 1;
  this.hidden = options.hidden;
}

Line.prototype.update = function() {
  var dx = this.p1.x - this.p0.x;
  var dy = this.p1.y - this.p0.y;
  var distance = Math.sqrt(dx * dx + dy * dy);
  var difference = this.length - distance;
  var percent = difference / distance * 0.5;
  var offsetX = dx * percent;
  var offsetY = dy * percent;

  if(!this.p0.fixed) {
    this.p0.x -= offsetX;
    this.p0.y -= offsetY;
  }

  if(!this.p1.fixed) {
    this.p1.x += offsetX;
    this.p1.y += offsetY;
  }
};

Line.prototype.render = function(context) {
  context.beginPath();
  context.strokeStyle = this.color;
  context.lineWidth = this.thickness;
  context.moveTo(this.p0.x, this.p0.y);
  context.lineTo(this.p1.x, this.p1.y);
  context.closePath();
  context.stroke();
};

module.exports = Line;