'use strict';

var Rectangle = require('../maths/rectangle');
var Point = require('./Point');
var Line = require('./Line');

function Tentacle(options) {
  this.bounce = options.bounce || 0.9;
  this.gravity = options.gravity || {
      x: 0,
      y: 0
    };
  this.friction = options.friction || 0.999;
  this.nbPoints = options.nbPoints || Math.random() * 40 + 10;
  this.distanceBetweenPoints = options.distanceBetweenPoints || 10;
  this.boundaries = options.boundaries || new Rectangle(0, 0, window.innerWidth, window.innerHeight);
  this.origin = options.origin || {
      x: this.boundaries.width * 0.5,
      y: this.boundaries.height * 0.5
    };
  this.angle = options.angle || Math.PI * 2 * Math.random();
  this.nbConstraintIterations = 1;

  this.points = [];
  this.sticks = [];

  this.createPoints();
}

Tentacle.prototype.createPoints = function() {
  for (var i = 0; i < this.nbPoints; i++) {
    this.points.push(new Point({
      x: this.origin.x + Math.cos(this.angle) * (i * this.distanceBetweenPoints),
      y: this.origin.y + Math.sin(this.angle) * (i * this.distanceBetweenPoints),
      oldx: i === 0 ? this.origin.x : this.origin.x + Math.cos(this.angle) * ((i - Math.random() * 0.5) * this.distanceBetweenPoints),
      oldy: i === 0 ? this.origin.y : this.origin.y + Math.sin(this.angle) * ((i - Math.random() * 0.5) * this.distanceBetweenPoints),
      fixed: i === 0
    }));
  }

  for (var i = 0, l = this.points.length - 1; i < l; i++) {
    this.sticks.push(new Line({
      p0: this.points[i],
      p1: this.points[i + 1]
    }));
  }
};

Tentacle.prototype.constrainPoints = function() {
  for (var i = 0; i < this.points.length; i++) {
    this.points[i].applyConstraints(this.boundaries, this.friction, this.bounce);
  }
};

Tentacle.prototype.updateSticks = function() {
  for (var i = 0; i < this.sticks.length; i++) {
    this.sticks[i].update();
  }
};

Tentacle.prototype.updatePoints = function() {
  for (var i = 0; i < this.points.length; i++) {
    this.points[i].update(this.friction, this.gravity);
  }
};

Tentacle.prototype.renderPoints = function(context) {
  for (var i = 0; i < this.points.length; i++) {
    this.points[i].render(context);
  }
};

Tentacle.prototype.renderSticks = function(context) {
  for (var i = 0; i < this.sticks.length; i++) {
    this.sticks[i].render(context);
  }
};

Tentacle.prototype.update = function() {
  this.updatePoints();

  for (var i = 0; i < this.nbConstraintIterations; i++) {
    this.updateSticks();
    this.constrainPoints();
  }
};

Tentacle.prototype.render = function(context) {
  this.renderSticks(context);
  this.renderPoints(context);
};

module.exports = Tentacle;