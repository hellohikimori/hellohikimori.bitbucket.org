'use strict';

var canvas;
var ctx;
var pattern;
var circle = {
  x: 0,
  y: 0,
  radius: 70,
  zoomed: false
};

(function() {
  canvas = document.getElementById('test');
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  ctx = canvas.getContext('2d');

  var img = new Image();
  img.src = './SS_OCS_HI_Ship_Night.jpg';
  img.width = canvas.width;
  img.height = canvas.height;
  img.onload = function() {
    var p = patternFromImage(img, canvas.width, canvas.height);
    pattern = ctx.createPattern(p, 'no-repeat');
    // console.log(img, pattern);
    addEvents();
    animate();
  };
})();

function patternFromImage(img, w, h) {
  var patternCanvas = document.createElement('canvas');
  patternCanvas.width = w;
  patternCanvas.height = h;
  var patternCtx = patternCanvas.getContext('2d');
  patternCtx.drawImage(img, 0, 0, w, h);

  return patternCanvas;
}

function addEvents() {
  canvas.addEventListener('click', function() {
    if (circle.zoomed) {
      TweenMax.killTweensOf(circle);
      circle.zoomed = false;
      TweenMax.to(circle, 0.6, {
        radius: 70,
        ease: Expo.easeOut
      });
    } else {
      TweenMax.killTweensOf(circle);
      circle.zoomed = true;
      TweenMax.to(circle, 0.6, {
        radius: 0.5 * canvas.width,
        ease: Expo.easeOut
      });
    }
  });
  canvas.addEventListener('mousemove', function(e) {
    if (circle.zoomed) return;

    circle.x = e.clientX;
    circle.y = e.clientY;
  });
}

function drawCircle(x, y, radius) {
  ctx.save();
  ctx.beginPath();

  ctx.fillStyle = pattern;
  ctx.arc(x, y, radius, 0, 2 * Math.PI);
  ctx.fill();

  ctx.closePath();
  ctx.restore();
}

function animate() {
  requestAnimationFrame(animate);

  ctx.clearRect(0, 0, canvas.width, canvas.height);
  drawCircle(circle.x, circle.y, circle.radius);
}
