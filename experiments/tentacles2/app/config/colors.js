module.exports = {
  /*
    Schemes
   */
  dark: ['#121212', '#232323', '#1A1A1A'],
  light: ['#212121', '#2A2A2A', '#252525'],
  goldDark: ['#a29569'],
  goldLight: ['#cabd91'],

  /*
    Methods
   */
  getRandomDarkColor: function() {
    return this.getRandomColor(this.dark);
  },
  getRandomLightColor: function() {
    return this.getRandomColor(this.light);
  },
  getRandomColor: function(scheme) {
    return scheme[Math.floor(Math.random() * scheme.length)];
  }
};