'use strict';

var Rectangle = require('../maths/rectangle');
var Point = require('./Point');
var Line = require('./Line');
var colors = require('../config/colors');
var copy = require('gl-vec2/copy');

function Tentacle(options) {
  
  /*
    Physics
   */
  this.gravity = options.gravity || [0, 0];
  this.bounce = options.bounce || 0.99999;
  this.friction = options.friction || 0.99999;
  this.boundaries = options.boundaries || new Rectangle(0, 0, window.innerWidth, window.innerHeight);

  /*
    Resolution
   */
  this.nbPoints = options.nbPoints || 50;
  this.distanceBetweenPoints = options.distanceBetweenPoints || 10;
  
  /*
    Appearance
   */
  this.colorSchemeDark = options.colorSchemeDark || colors.dark;
  this.colorSchemeLight = options.colorSchemeLight || colors.dark;
  this.alpha = options.alpha || 1;
  this.colorDark = colors.getRandomColor(this.colorSchemeDark);
  this.colorLight = colors.getRandomColor(this.colorSchemeLight);
  this.thickness = options.thickness || 10;

  /*
    Positioning
   */
  this.position = options.position || [0, 0];
  this.angle = options.angle || Math.PI * 2 * Math.random();

  /*
    Content
   */
  this.points = [];
  this.lines = [];
  this.normals = [];

  /*
    Populate
   */
  this.createPoints();
  this.createLines();
}

/* =======================================================================
  POPULATING
======================================================================= */
Tentacle.prototype.createPoints = function() {
  for (var i = 0; i < this.nbPoints; i++) {
    this.points.push(new Point({
      position: [
        this.position[0] + Math.cos(this.angle) * (i * this.distanceBetweenPoints),
        this.position[1] + Math.sin(this.angle) * (i * this.distanceBetweenPoints)
      ],
      oldPosition: [
        i === 0 ? this.position[0] : this.position[0] + Math.cos(this.angle) * ((i - Math.random() * 0.5) * this.distanceBetweenPoints),
        i === 0 ? this.position[1] : this.position[1] + Math.sin(this.angle) * ((i - Math.random() * 0.5) * this.distanceBetweenPoints)
      ],
      fixed: i === 0
    }));
  }
  return this.points;
};

Tentacle.prototype.createLines = function() {
  for (var i = 0, l = this.points.length - 1; i < l; i++) {
    this.lines.push(new Line({
      p0: this.points[i],
      p1: this.points[i + 1],
      thickness: this.thickness * ((l - i) / l)
    }));
  }
  return this.lines;
};

/* =======================================================================
  UPDATING (physics and all)
======================================================================= */
Tentacle.prototype.constrainPoints = function() {
  for (var i = 0; i < this.points.length; i++) {
    this.points[i].applyConstraints(this.boundaries, this.friction, this.bounce);
  }
};

Tentacle.prototype.updateLines = function() {
  for (var i = 0; i < this.lines.length; i++) {
    this.lines[i].update();
  }
};

Tentacle.prototype.updatePoints = function() {
  for (var i = 0; i < this.points.length; i++) {
    this.points[i].update(this.friction, this.gravity);
  }
};

Tentacle.prototype.computeNormals = function() {
  this.normals = [];
  for(var i = 0, line0, line1; i < this.lines.length - 1; i++) {
    line0 = this.lines[i];
    line1 = this.lines[i + 1];

    this.normals.push([
      [
        line1.normalStartOrigin[0] + (line0.normalEndOrigin[0] - line0.p1.position[0]),
        line1.normalStartOrigin[1] + (line0.normalEndOrigin[1] - line0.p1.position[1])
      ],
      [
        line1.normalStartEnd[0] + (line0.normalEndEnd[0] - line0.p1.position[0]),
        line1.normalStartEnd[1] + (line0.normalEndEnd[1] - line0.p1.position[1])
      ]
    ]);
  }
  
  return this.normals;
};

Tentacle.prototype.update = function() {
  this.updatePoints();
  this.updateLines();
  this.constrainPoints();
  this.computeNormals();
};

/* =======================================================================
  RENDERING (actually displaying something)
======================================================================= */
Tentacle.prototype.render = function(context) {
  /*
    Save and apply alpha
   */
  var previousAlpha = context.globalAlpha;
  context.globalAlpha = this.alpha;

  /*
    Draw dark half of the tentacle
   */
  context.beginPath();
  context.fillStyle = this.colorDark;
  context.moveTo(this.lines[0].p0.position[0], this.lines[0].p0.position[1]);
  for(var i = 0, l = this.normals.length; i < l; i++) {
    context.lineTo(this.normals[i][0][0],this.normals[i][0][1]);
  }
  context.lineTo(this.lines[this.lines.length - 1].p1.position[0], this.lines[this.lines.length - 1].p1.position[1]);
  for(var i = this.lines.length - 1; i >= 0; i--) {
    context.lineTo(this.lines[i].p0.position[0], this.lines[i].p0.position[1])
  }
  context.closePath();
  context.fill();

  /*
    Draw light half of the tentacle
   */
  context.beginPath();
  context.fillStyle = this.colorLight;
  context.moveTo(this.lines[0].p0.position[0], this.lines[0].p0.position[1]);
  for(var i = 0, l = this.normals.length; i < l; i++) {
    context.lineTo(this.normals[i][1][0],this.normals[i][1][1]);
  }
  context.lineTo(this.lines[this.lines.length - 1].p1.position[0], this.lines[this.lines.length - 1].p1.position[1]); 
  for(var i = this.lines.length - 1; i >= 0; i--) {
    context.lineTo(this.lines[i].p0.position[0], this.lines[i].p0.position[1])
  }
  context.closePath();
  context.fill();

  /*
    Restore alpha
   */
  context.globalAlpha = previousAlpha;
};

module.exports = Tentacle;