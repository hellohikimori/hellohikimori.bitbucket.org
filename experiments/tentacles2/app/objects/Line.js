'use strict';

var distance = require('gl-vec2/distance');
var normalize = require('gl-vec2/normalize');
var scale = require('gl-vec2/scale');
var Point = require('./Point');

function Line(options) {
  this.p0 = options.p0 || new Point();
  this.p1 = options.p1 || new Point();
  this.thickness = options.thickness || 1;
  this.length = distance(this.p0.position, this.p1.position);
}

Line.prototype.update = function() {
  var dx = this.p1.position[0] - this.p0.position[0];
  var dy = this.p1.position[1] - this.p0.position[1];
  var distance = Math.sqrt(dx * dx + dy * dy);
  var difference = this.length - distance;
  var percent = difference / distance * 0.5;
  var offsetX = dx * percent;
  var offsetY = dy * percent;

  if(!this.p0.fixed) {
    this.p0.position[0] -= offsetX;
    this.p0.position[1] -= offsetY;
  }

  if(!this.p1.fixed) {
    this.p1.position[0] += offsetX;
    this.p1.position[1] += offsetY;
  }

  this.computeNormals();
};

Line.prototype.computeNormals = function() {
  var d = normalize([0, 0], [
    this.p1.position[0] - this.p0.position[0],
    this.p1.position[1] - this.p0.position[1]
  ]);

  scale(d, d, this.thickness);

  this.normalStartOrigin = [
    this.p0.position[0] - d[1],
    this.p0.position[1] + d[0]
  ];
  this.normalStartEnd = [
    this.p0.position[0] + d[1],
    this.p0.position[1] - d[0]
  ];

  this.normalEndOrigin = [
    this.p1.position[0] - d[1],
    this.p1.position[1] + d[0]
  ];
  this.normalEndEnd = [
    this.p1.position[0] + d[1],
    this.p1.position[1] - d[0]
  ];
};

module.exports = Line;