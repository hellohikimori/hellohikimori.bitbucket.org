'use strict';

var Tentacle = require('./Tentacle');
var Rectangle = require('../maths/rectangle');
var colors = require('../config/colors');

function Beast(options) {
  this.nbTentacles = options.nbTentacles || 16;
  this.coreRadius = options.coreRadius || 0;
  this.boundaries = options.boundaries || new Rectangle(0, 0, window.innerWidth, window.innerHeight);
  this.position = options.position || [
    this.boundaries.x + this.boundaries.width * 0.5,
    this.boundaries.y + this.boundaries.height * 0.5
  ];
  
  this.tentacles = [];

  for(var i = 0, angle, isSmall; i < this.nbTentacles; i++) {
    angle = Math.random() * Math.PI + i + 1 * (Math.PI * 2 / this.nbTentacles);
    isSmall = Math.random() < 0.2;
    this.tentacles.push(new Tentacle({
      angle: angle,
      boundaries: this.boundaries,
      thickness: isSmall ? 2 : Math.random() * 8 + 5,
      colorSchemeDark: isSmall ? colors.goldDark : colors.dark,
      colorSchemeLight: isSmall ? colors.goldLight : colors.light,
      nbPoints: Math.random() * 40 + 30,
      position: [
        this.position[0] + Math.cos(angle) * this.coreRadius,
        this.position[1] + Math.sin(angle) * this.coreRadius
      ]
    }));
  }
}
 
Beast.prototype.update = function(context) {
  for(var i = 0; i < this.nbTentacles; i++) {
    this.tentacles[i].update();
    this.tentacles[i].render(context);
  }
};

module.exports = Beast;