'use strict';

var copy = require('gl-vec2/copy');
var add = require('gl-vec2/add');

function Point(options) {
  this.position = options.position || [0, 0];
  this.oldPosition = options.oldPosition || [
    this.position[0] + Math.random() * 20 - 10,
    this.position[1] + Math.random() * 20 - 10
  ];
  this.offset = options.offset || [
    Math.random() * 8 - 4,
    Math.random() * 8 - 4
  ];

  this.fixed = options.fixed;
}

Point.prototype.update = function(friction, gravity) {
  if(this.fixed) return;

  var v = [
    (this.position[0] - this.oldPosition[0]) * friction,
    (this.position[1] - this.oldPosition[1]) * friction,
  ];

  copy(this.oldPosition, this.position);

  add(this.position, this.position, v);
  add(this.position, this.position, gravity);
};

Point.prototype.applyConstraints = function(boundaries, friction, bounce) {
  if(this.fixed) return;

  var v = [
    (this.position[0] - this.oldPosition[0]) * friction,
    (this.position[1] - this.oldPosition[1]) * friction,
  ];

  if(this.position[0] > boundaries.width) {
    this.position[0] = boundaries.width;
    this.oldPosition[0] = this.position[0] + v[0] * bounce;
  }
  else if(this.position[0] < 0) {
    this.position[0] = 0;
    this.oldPosition[0] = this.position[0] + v[0] * bounce;
  }
  if(this.position[1] > boundaries.height) {
    this.position[1] = boundaries.height;
    this.oldPosition[1] = this.position[1] + v[1] * bounce;
  }
  else if(this.position[1] < 0) {
    this.position[1] = 0;
    this.oldPosition[1] = this.position[1] + v[1] * bounce;
  }
};

module.exports = Point;