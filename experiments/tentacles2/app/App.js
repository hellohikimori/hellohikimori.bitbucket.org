'use strict'

var raf = require('raf');
var Rectangle = require('./maths/rectangle');
var Tentacle = require('./objects/Tentacle');
var Beast = require('./objects/Beast');

function App() {
  this.canvas = document.getElementById("canvas");
  this.context = this.canvas.getContext("2d");
  this.canvas.width = window.innerWidth;
  this.canvas.height = window.innerHeight;
  this.boundaries = new Rectangle(0, 0, this.canvas.width, this.canvas.height);

  this.beast = new Beast({
    boundaries: this.boundaries
  });
  
  raf(this.update.bind(this));
}

App.prototype.clearCanvas = function() {
  this.context.globalAlpha = 0.2;
  this.context.fillStyle = '#FFFFFF';
  this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
  this.context.globalAlpha = 1;
};
 
App.prototype.update = function() {
  raf(this.update.bind(this));

  this.clearCanvas();
  this.beast.update(this.context);
};

module.exports = App;