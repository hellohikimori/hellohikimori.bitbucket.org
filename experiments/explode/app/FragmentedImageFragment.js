'use strict'

var inherits = require('inherits');
var PIXI = require('pixi.js');

function FragmentedImageFragment(options) {
  PIXI.DisplayObjectContainer.call(this);

  this.texture = options.texture;
  this.points = options.points;
  this.reference = options.reference;

  if(options.useBlur) {
    this.blur = new PIXI.BlurFilter();
    this.blur.blur = 0;
    this.filters = [this.blur];
  }

  this.origin = [
    (this.points[0][0] + this.points[1][0] + this.points[2][0] ) / 3,
    (this.points[0][1] + this.points[1][1] + this.points[2][1] ) / 3
  ];

  this.dest = [
    Math.random() * 0.5 * (this.origin[0] - this.reference[0]),
    Math.random() * 0.5 * (this.origin[1] - this.reference[1])
  ];

  this.graphics = new PIXI.Graphics();
  this.graphics.beginFill(0xFFFFFF * Math.random());
  this.graphics.moveTo(this.points[0][0], this.points[0][1]);

  for(var i = 1, l = this.points.length; i < l; i++) {
    this.graphics.lineTo(this.points[i][0], this.points[i][1]);
  }

  this.graphics.lineTo(this.points[0][0], this.points[0][1]);
  this.graphics.endFill();

  this.addChild(this.graphics);

  this.image = new PIXI.Sprite(this.texture);
  this.addChild(this.image);
  this.image.mask = this.graphics;


}

inherits(FragmentedImageFragment, PIXI.DisplayObjectContainer);

module.exports = FragmentedImageFragment;