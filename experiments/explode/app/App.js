'use strict'

var raf = require('raf');
var FragmentedImage = require('./FragmentedImage');
var resize = require('brindille-resize');
var PIXI = require('pixi.js');

function App() {
  this.renderer = new PIXI.WebGLRenderer(1280, 720);
  this.stage = new PIXI.Stage(0xFFFFFF);

  var nbCol = Math.ceil(window.innerWidth / 480);

  for(var i = 0, l = 16; i < l; i++) {
    var image = new FragmentedImage({
      url: 'assets/images/img' + i + '.jpg'
    });
    image.x = image.imageWidth * (i % nbCol);
    image.y = image.imageHeight * Math.floor(i / nbCol);
    this.stage.addChild(image);
  }

  resize.addListener(this.resize.bind(this));

  raf(this.animate.bind(this));
}

App.prototype.resize = function() {
  this.renderer.resize(resize.width, resize.height);

  // this.image.x = this.renderer.width * 0.5 - this.image.imageWidth * 0.5;
  // this.image.y = this.renderer.height * 0.5 - this.image.imageHeight * 0.5;
}

App.prototype.animate = function() {
  raf(this.animate.bind(this));

  this.renderer.render(this.stage);
};

module.exports = App;