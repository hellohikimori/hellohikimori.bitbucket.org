'use strict'

var inherits = require('inherits');
var Delaunay = require('delaunay-fast');
var PIXI = require('pixi.js');
var Fragment = require('./FragmentedImageFragment');

function FragmentedImage(options) {
  PIXI.DisplayObjectContainer.call(this);

  this.url = options.url;
  this.nbPoints = options.nbPoints || 10;
  this.imageWidth = options.imageWidth || 480;
  this.imageHeight = options.imageHeight || 270;
  this.origin = [this.imageWidth * 0.5, this.imageHeight * 0.5];

  this.texture = PIXI.Texture.fromImage(options.url);
  this.randomPoints = [[0, 0], [this.imageWidth * 0.5, 0], [this.imageWidth, 0], [this.imageWidth, this.imageHeight * 0.5], [this.imageWidth, this.imageHeight], [this.imageWidth * 0.5, this.imageHeight], [0, this.imageHeight], [0, this.imageHeight * 0.5]];

  for (var i = 0; i < this.nbPoints; i++) {
    this.randomPoints.push([
      Math.random() * this.imageWidth,
      Math.random() * this.imageHeight
    ]);
  }

  this.triangles = Delaunay.triangulate(this.randomPoints);
  this.fragments = [];
  this.tl = new TimelineMax();

  this.createFragments(this.randomPoints, this.triangles);

  this.placeHolder = new PIXI.Sprite();
  this.placeHolder.setTexture(this.texture);
  this.addChildAt(this.placeHolder, 0);
  this.placeHolder.interactive = true;
  this.placeHolder.mouseover = this.onMouseOver.bind(this);
  this.placeHolder.mouseout = this.onMouseOut.bind(this);

  this.tl.set(this.placeHolder, {alpha: 0}, 0.0001);

  this.tl.pause(0);
}

inherits(FragmentedImage, PIXI.DisplayObjectContainer);

FragmentedImage.prototype.createFragments = function(points, triangles) {
  var fragment;
  for (var i = 0, l = triangles.length; i < l; i += 3) {

    fragment = new Fragment({
      texture: this.texture,
      reference: this.origin,
      useBlur: Math.random() < 0.1,
      points: [
        points[triangles[i + 0]],
        points[triangles[i + 1]],
        points[triangles[i + 2]]
      ]
    });
    this.fragments.push(fragment);
    this.addChild(fragment);
    this.tl.to(fragment, 0.6, {
      x: fragment.dest[0],
      y: fragment.dest[1],
      ease: Expo.easeOut
    }, Math.random() * 0.2);
    if(fragment.blur) {
      this.tl.to(fragment.blur, 0.6, {
        blur: 8,
        ease: Expo.easeOut
      }, Math.random() * 0.2);
    }
  }
};

FragmentedImage.prototype.onMouseOver = function(data) {
  // this.placeHolder.alpha = 0;
  console.log('mouseover', this.parent.setChildIndex(this, this.parent.children.length -1));
  this.tl.timeScale(1);
  this.tl.play();
};

FragmentedImage.prototype.onMouseOut = function(data) {
  // this.placeHolder.alpha = 1;
  this.tl.timeScale(2);
  this.tl.reverse();
};

module.exports = FragmentedImage;