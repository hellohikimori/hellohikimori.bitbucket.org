'use strict'

var domready = require('domready');
var App = require ('./app/App');
var gsap = require('gsap');

domready(function() {
  var app = new App();
  document.body.appendChild(app.renderer.view);
});
