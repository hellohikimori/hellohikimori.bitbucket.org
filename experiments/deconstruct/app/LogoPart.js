'use strict'

var inherits = require('inherits');
var Delaunay = require('delaunay-fast');
var inside = require('point-in-polygon');
var PIXI = require('pixi.js');

function LogoPart(points, color, logoWidth, logoHeight) {
  PIXI.DisplayObjectContainer.call(this);

  this.points = points;
  this.logoWidth = logoWidth;
  this.logoHeight = logoHeight;
  this.colorBack = color;

  this.back = new PIXI.Graphics();
  this.back.beginFill(this.colorBack);
  this.back.moveTo(points[0][0] * this.logoWidth, points[0][1] * this.logoHeight);

  for(var i = 0, l = points.length; i < l; i++) {
    this.back.lineTo(points[i][0] * this.logoWidth, points[i][1] * this.logoHeight);
  }

  this.back.lineTo(points[0][0] * this.logoWidth, points[0][1] * this.logoHeight);
  this.back.endFill();
  this.addChild(this.back);


  this.computeBoundingRect();

  for(var i = 0; i < 100; i++) {
    this.points.push(this.getRandomPoint());
  }

  var trianglesData = Delaunay.triangulate(this.points);

  this.triangles = [];

  for(var i = 0, l = trianglesData.length; i < l; i += 3) {
    var g = new PIXI.Graphics();
    g.beginFill(this.colorBack);
    g.moveTo(this.points[trianglesData[i + 0]][0] * this.logoWidth, this.points[trianglesData[i + 0]][1] * this.logoHeight);
    g.lineTo(this.points[trianglesData[i + 1]][0] * this.logoWidth, this.points[trianglesData[i + 1]][1] * this.logoHeight);
    g.lineTo(this.points[trianglesData[i + 2]][0] * this.logoWidth, this.points[trianglesData[i + 2]][1] * this.logoHeight);
    g.lineTo(this.points[trianglesData[i + 0]][0] * this.logoWidth, this.points[trianglesData[i + 0]][1] * this.logoHeight);
    g.endFill();
    this.addChild(g);
    this.triangles.push(g);
  }
}

inherits(LogoPart, PIXI.DisplayObjectContainer);

LogoPart.prototype.computeBoundingRect = function() {
  this.min = [99999, 99999];
  this.max = [-1, -1];
  for(var i = 0, l = this.points.length; i < l; i++) {
    if(this.points[i][0] <= this.min[0]) this.min[0] = this.points[i][0];
    if(this.points[i][1] <= this.min[1]) this.min[1] = this.points[i][1];
    if(this.points[i][0] >= this.max[0]) this.max[0] = this.points[i][0];
    if(this.points[i][1] >= this.max[1]) this.max[1] = this.points[i][1];
  }
};

LogoPart.prototype.getRandomPoint = function() {
  var p = [
    this.min[0] + Math.random() * (this.max[0] - this.min[0]),
    this.min[1] + Math.random() * (this.max[1] - this.min[1])
  ];
  var isInside = inside(p, this.points);
  if(isInside) {
    return p;
  }
  else {
    return this.getRandomPoint();
  }
};

module.exports = LogoPart;