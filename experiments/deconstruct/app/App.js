'use strict'

var raf = require('raf');
var Logo = require('./Logo');
var resize = require('brindille-resize');
var PIXI = require('pixi.js');

function App() {
  this.renderer = new PIXI.WebGLRenderer(1280, 720, {antialias: true});
  this.stage = new PIXI.Stage(0xFFFFFF);

  this.logo = new Logo();
  this.stage.addChild(this.logo);


  resize.addListener(this.resize.bind(this));

  raf(this.animate.bind(this));
}

App.prototype.resize = function() {
  this.renderer.resize(resize.width, resize.height);

  this.logo.x = this.renderer.width * 0.5 - this.logo.logoWidth * 0.5;
  this.logo.y = this.renderer.height * 0.5 - this.logo.logoHeight * 0.5;
}

App.prototype.animate = function() {
  raf(this.animate.bind(this));

  this.renderer.render(this.stage);
};

module.exports = App;