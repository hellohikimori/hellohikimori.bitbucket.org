'use strict'

var inherits = require('inherits');
var Delaunay = require('delaunay-fast');
var PIXI = require('pixi.js');
var LogoPart = require('./LogoPart');

function Logo() {
  PIXI.DisplayObjectContainer.call(this);

  var coords = [
    {
      color: 0x434343,
      points: [
        [0, 1],
        [0, 0.597083426],
        [0.327888845, 0],
        [0.549196394, 0]
      ]
    },
    {
      color: 0x434343,
      points: [
        [0.248442876, 1],
        [0.248442876, 0.597083426],
        [0.576636613, 0],
        [0.797900605, 0]
      ]
    },
    {
      color: 0x656565,
      points: [
        [0.435646152, 0.659264194],
        [0.543098567, 0.463764897],
        [0.797900605, 1],
        [0.590661614, 1]
      ]
    },
    {
      color: 0x656565,
      points: [
        [0.81689098, 0],
        [1, 0],
        [0.81689098, 0.333333333]
      ]
    },
    {
      color: 0x434343,
      points: [
        [0.81689098, 0.333333333],
        [1, 0],
        [1, 1],
        [0.81689098, 1]
      ]
    }
  ];

  this.logoWidth = 229.59;
  this.logoHeight = 135.09;

  var background = new PIXI.Graphics();
  background.beginFill(0xFF0000, 0);
  background.drawRect(0, 0, this.logoWidth, this.logoHeight);
  this.addChild(background);

  background.interactive = true;
  background.mouseover = this.onMouseOver.bind(this);
  background.mouseout = this.onMouseOut.bind(this);

  this.parts = [];

  for (var i = 0, l = coords.length; i < l; i++) {
    var part = new LogoPart(coords[i].points, coords[i].color, this.logoWidth, this.logoHeight);
    this.addChild(part);
    this.parts.push(part);
  }

  // var pixelateFilter = new PIXI.PixelateFilter();
  // pixelateFilter.size.x = 5;
  // pixelateFilter.size.y = 5;
  // this.filters = [pixelateFilter];
}

inherits(Logo, PIXI.DisplayObjectContainer);

Logo.prototype.onMouseOver = function() {
  var part;

  if (this.tl) this.tl.kill();
  this.tl = new TimelineMax();

  for (var i = 0, l = this.parts.length; i < l; i++) {
    part = this.parts[i];
    part.back.alpha = 0;
    for (var j = 0, k = part.triangles.length; j < k; j++) {
      part.triangles[j].alpha = 1;
      this.tl.to(part.triangles[j], 2, {
        alpha: Math.random() * 0.5 + 0.5,
        x: Math.random() * 100 - 50,
        y: Math.random() * 100 - 50,
        rotation: 0.2 * (Math.random() * Math.PI * 2 - Math.PI),
        ease: Elastic.easeOut
      }, Math.random() * 0.2);
    }
  }

  this.tl.play();
};

Logo.prototype.onMouseOut = function() {
  var part;

  if (this.tl) this.tl.kill();
  this.tl = new TimelineMax({
    onComplete: function() {
      for (var i = 0, l = this.parts.length; i < l; i++) {
        this.parts[i].back.alpha = 1;
        for (var j = 0, k = this.parts[i].triangles.length; j < k; j++) {
          this.parts[i].triangles[j].alpha = 0;
        }
      }
    },
    onCompleteScope: this
  });

  for (var i = 0, l = this.parts.length; i < l; i++) {
    part = this.parts[i];
    for (var j = 0, k = part.triangles.length; j < k; j++) {
      this.tl.to(part.triangles[j], 0.5, {
        alpha: 1,
        x: 0,
        y: 0,
        rotation: 0,
        ease: Expo.easeOut
      }, Math.random() * 0.2);
    }
  }
};

module.exports = Logo;